## Environment

### Beachhead 

`bchd` is a terminal environment provided via a browser tab.
By navigating to the `$FQDN` where the environment is hosted the student will imediately get a shell access.

This terminal is setup to run tmux by default for peer colaboration and maintaining state across refreshes.
 - For additional tmux terminal windows push <kbd>Ctrl</kbd>+<kbd>b</kbd> then press <kbd>c</kbd> (create)
 - To switch between tmux terminal windows push <kbd>Ctrl</kbd>+<kbd>b</kbd> then press <kbd>n</kbd> (next)

### Networks
| Name     | Subnet/Mask      | 
|----------|------------------|
| internet | 200.200.200.0/24 |
| novice   | 172.16.2.0/24    |
| expert   | 10.10.10.0/24    |
| master   | 192.168.200.0/24 |

### Hosts
| Name            | First Interface | Second Interface |
|-----------------|-----------------|------------------|
| beachheads      | 200.200.200.X   |                  |
| redirectors     | 200.200.200.X   |                  |
| router-firewall | 172.16.2.1      | 200.200.200.1    |
| server-1        | 172.16.2.100    | 10.10.10.44      | 
| server-2        | 10.10.10.55     | 192.168.200.66   |
| server-3        | 192.168.200.89  |                  |


## Challenges

### So, you're finally awake

> Your adventure begins on `bchd`, your gateway into the unknown.
> 
> To get you started, there is a flag somewhere in your home directory. Enter it here!

**Local environment**:
 - ttyd, 200.200.200.2 - only you in this environment - not shared between teams
 - All tools will need installed by the student.  A good starting point to reinforce this is
   to have the students install man and less and enable the man pages:

   ``` bash
   apt update
   apt install -y man less
   unminimize
   ```

**How to submit flags**:
 - The first FLAG is in ~/flag.txt `{FLAG:59797335-6254-40bd-813b-e3f655f702c3}`
 - This flag ensures students understand how to submit a flag:
   - Yes put in the full contents between the `{}`'s
   - Yes also include the `{}`'s

**Hints**:
 - Worth noting now is that some challenges have hints that cost 0 points
 - Some challenges are not solveable without using the hints
 - The expectation is that students spend no more than 10 or 15 minutes thinking about 
   a challenge before using each hint that is provided.


### Into the Unknown

> We're gonna need some redirectors if we're gonna make it safely to the championships.
>
> Our pre-mission practice rounds have revealed a number of suitable redirectors on the 200.200.200.0/24 subnet. They're ready for you, listening on port 1337.
> 
> The password is: pawn

 - Scan for redirectors

   ```
   apt update
   apt install -y nmap
   nmap -p 1337 --open -Pn -sV 200.200.200.1/24
   ```

 - Example nmap scan output

   ``` bash
   root@bchd:~# nmap -p 1337 --open -Pn -sV 200.200.200.1/24
   Starting Nmap 7.80 ( https://nmap.org ) at 2022-01-08 16:51 UTC
   Nmap scan report for range_redirector1_1.range_simnet (200.200.200.132)
   Host is up (0.000013s latency).
   
   PORT     STATE SERVICE VERSION
   1337/tcp open  ssh     OpenSSH 8.2p1 Ubuntu 4ubuntu0.4 (Ubuntu Linux; protocol 2.0)
   MAC Address: 02:42:C8:C8:C8:84 (Unknown)
   Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
   
   Nmap scan report for range_redirector2_1.range_simnet (200.200.200.199)
   Host is up (0.000013s latency).
   
   PORT     STATE SERVICE VERSION
   1337/tcp open  ssh     OpenSSH 8.2p1 Ubuntu 4ubuntu0.4 (Ubuntu Linux; protocol 2.0)
   MAC Address: 02:42:C8:C8:C8:C7 (Unknown)
   Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
   
   Nmap scan report for range_redirector3_1.range_simnet (200.200.200.224)
   Host is up (0.000012s latency).
   
   PORT     STATE SERVICE VERSION
   1337/tcp open  ssh     OpenSSH 8.2p1 Ubuntu 4ubuntu0.4 (Ubuntu Linux; protocol 2.0)
   MAC Address: 02:42:C8:C8:C8:E0 (Unknown)
   Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
   
   Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
   Nmap done: 256 IP addresses (7 hosts up) scanned in 2.24 seconds
   ```

 - ssh to the redirector
 
   ```
   ssh root@200.200.200.132 -p 1337
   # password: pawn
   cat /root/flag.txt
   # {FLAG:91596f7d-88f1-454f-82ba-3d61c48b2e73}
   ```

### Opening Moves

> There is a  map on `bchd`.  Use it to gain access to the `router-firewall`

 - Given what we know from the prompt indicating a "map", do a search on the redirector filesystem

   ``` bash 
   # on bchd
   find / -type f -name *map*
   ```

 - `map.txt` is a pem formatted private key
 - File permissions will need to be updated in order to use the ssh key file
 - Teachable moment: Needing to modify the file permissions is intentional.  Why do file permissions matter?
   
   ``` bash
   chmod 0400 /map.txt
   ```

 - Use `ssh-keygen` to derive the username and ip on `router-firewall`
 
   ``` bash
   ssh-keygen -y -f /map.txt
   ``` 

 - Example output from `ssh-keygen`

   ``` bash
   ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDLR4CQwDliKoDnDwHju76B7In0BtcYYCZB3TzywDawFs2ejy9Lu/BOU4V84gvyrkdKU1HTaePI2CDQq2uRxDPFDBhp61TSIh8nxPQVs/atWCy46sc9R9E/feTWXeqPuK5IXXzLq0B63tVew12wShaQZRvV+WTqDdKJZjBqep5lSmQDLeNcUVYq3NC9+gh9c5nHmXSr/H0wiYriLF/uO+xHb007jNGJGALwqrgs/6FhinCkvOATuzOcpEshWlUxrUDCTOnMf+V7QLoLkROn/8AOg+8+lRsUuRPyesNohL+/gnORGAwHqjPVgFuP1q37d+ivTKnq/wymojLUwoWJw9NXws/+pYGxJId53iCZnlmU8n6dHJwTs/c9KxI3aLLiBtPeLvxSYWzWLIwcvrkul5bRGI9rI1+wGdVt44SJA4VveL/wP8gJyYTynmTqLe3pHHJ0sXxFTzpl6KNZ1KyL5p4ymdy/t74sDKP0Kz4yVSNB2nq0rWNG9tx+SdCLTd4SmGM= admin@200.200.200.111
   ```

 - Connect to the firewall **using a redirector**

   ``` bash
   ssh root@200.200.200.132 -p 1337 -qNfL 127.0.0.1:2111:200.200.200.111:22
   ss -antp | grep 2111 # shows port forward listener
   ssh -i /map.txt admin@127.0.0.1 -p 2111
   ```

 - The login works! But we are not given a shell :(

   ``` 
   root@bchd:~# ssh -i /map.txt admin@127.0.0.1 -p 2111
   <-- snip Ubuntu MOTD -->
   Login sessions are disabled!
   We are super secure here at 1.d4 Chess Inc! SHOO!
   {FLAG:4636fdbb-adc1-48f1-8b3e-d8657dea2ba0}
   Connection to 127.0.0.1 closed.
   ```

 - Teachable moments: 
   - Did you forget to use the redirector? Why is that bad?
   - Did you move the private key to the redirector? Why is that bad?

### Novice Tournament

> You've shown your skills are worthy of competing in your first tournament. Find your way to the novice server.
> 
> That firewall is connected to an internal network: 172.16.2.0/24
> 
> Attached is the key to enter the competition.  We're pretty sure they are using port 2222 but we have no idea where they are running it.
> 
> NOTE: Please use the hint to get the target IP, this version of the training does not include proxy scanning.

Hint 1: 

> Proxychains scan result: 
> The next server is at 172.16.2.31 listening on port 2222

Hint 2: 

> Use `ssh-keygen` to gain insight into the  additional data fields of this private key

 - 🚀Rocket science lab: proxychains scan through the ssh connection   
 - Teachable moments: 
   - Why did we need to tell nmap to use -sT? 
   - What ip's would show an open port if we didnt?

   ``` bash
   # on bchd, 
   apt install -y proxychains4

   # assumses previous tunnels are up, dynamic port forward for scanning
   ssh -i /map.txt admin@127.0.0.1 -p 2111 -qNfD 9050 
   proxychains4 -q nmap -p 2222 -T5 -Pn -sT 172.16.2.0/24 

   # Too SLOW? use xargs!
   seq 1 254 | xargs -I {} -P0 proxychains4 -q nc -nvz 172.16.2.{} 2222 2>&1 | grep -v refused
   ```

 - ssh connect to the tournament

   ``` bash
   ssh -i /map.txt admin@127.0.0.1 -p 2111 -NfL 127.0.0.1:2222:172.16.2.31:2222
   ss -antp | grep 2222
   # key.pem file provided via ctfd
   ssh wshaibel@127.0.0.1 -p 2222 -i key.pem
   ```

 - Upon connection MOTD includes a flag

   ```
   root@bchd:~# ssh wshaibel@127.0.0.1 -p 2222 -i key.pem
   <-- snip ubuntu MOTD -->
   {FLAG:dc22e9db-9225-4082-b7ed-78f08784fd54}
   ```

### Novice - Game 1

> There's something not quite right about this opponent.  They keep castling back and forth over and over.
> 
> Turns out they are also using a weird  `sshd` configuration.

 - Find the sshd config file and look at it
 - Teachable moments:
   - What are GatewayPorts?
   - What would the be useful for?

   ``` bash
   cat /etc/ssh/sshd_config | grep GatewayPorts 
   # GatewayPorts yes #{FLAG:e9d5e38d-8179-4e06-93ca-d5cce06c2099}
   ```

### Novice - Game 2

> This player keeps using the same tatics over and over.
> Once you figure it out they wil be easy to defeat.
>
> They are sending their messages to all the ports in the range `1031-1225` on `server_1`.
> Do what you can do retrieve it back home - securely.

 - Setup a remote listener (`-R`) which uses GatewayPorts to listen on `0.0.0.0`
 - Any port in the range 1031 to 1225 will work

   ``` bash
   ssh wshaibel@127.0.0.1 -p 2222 -i key.pem -NfR 0.0.0.0:1031:127.0.0.1:1031 
   ps aux | grep 1031 
   apt install -y netcat
   nc -nvl 1031 | tee game-2.txt
   # within 60s data should be received 
   ```

 - Example output inside game-2.txt and stdout

   ```
   root@bchd:~# nc -nvl 1031 | tee game-2.txt
   Listening on 0.0.0.0 1031
   Connection received on 127.0.0.1 36580
   {FLAG:96239742-a253-40e8-bbb0-fba44b230cee}
   
   -----BEGIN OPENSSH PRIVATE KEY-----
   <-- snup key material -->
   -----END OPENSSH PRIVATE KEY-----
   ```

### Novice - Game 3

> There is an evidence file we need on `server_1`. 
> By any means nessisary, get this file and examine it's contents. 
>
> PW: 123456

Hint:

> The `base64` command can transform this file into a copy-paste-able format

 - The `unzip` utilitiy isnt installed on `server_1`
 - We need a way to get data back to bchd where we can install things

   ``` bash
   # on server_1
   base64 evidence_found_lens.enc > evidence_found_lens.enc.b64
   cat evidence_found_lens.enc.b64
   # copy back the b64 cotents to bchd to file evidence_found_lens.enc.b64

   # on bchd
   base64 -d evidence_found_lens.enc.b64 > evidence_found_lens.enc
   file evidence_found_lens.enc # zip
   apt install -y unzip
   unzip evidence_found_lens.enc
   # Password 123456
   cat evidence_found_lens.txt # Novice - Game 3
   ```

 - Teachable moments:
   - Did you remove any files created on server_1?
   - If not, why could that be bad?


### Expert Tournament

> The data you recieved after defeating the most difficult opponent at the novice level
> should have given you all required details to find the Expert tournament.
> 
> Good luck!

 - This prompt is referering to the private key we capture via the netcat listener
 - Run the same `ssh-keyscan` commands to gather info from the private key comments
 - First edit the file to not have the flag in it

   ``` bash
   chmod 0400 game-2.txt
   ssh-keygen -y -f game-2.txt
   ```

 - Example output from `ssh-keyscan`

   ``` 
   root@bchd:~# ssh-keygen -y -f game-2.txt
   ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDZS4XWPKCLkJ0PvWDtaAi7BzgeTgWeTEcUx5Tq5MpbC9oFKVSwTbvN8lcvkOO1pwJbW3NMrbNyt1magU32TLYzaTMeADF1UmLCI1Z5+osbyyTdyUlEyTNbgQQjhonRVRufxiangvh4Qk1NLTY5l/rCK4J7WOZoX3z3IAA0soyEm1652OinhtxxxRLutb3hG6lkYOpaeb4xzW6sZWBr8YOcuf04T35CxwKRuQ9+63qbC2RS3n8Q/Y1M441xrTlb+gIGJTD1J+uz1grar+VhTCXdQBRF7N8SE0vBXvDeHRyqQwZvKnRun55uf1n4/S+JHPwAG7NRxMdLT4BWDHrSk4BCAyGmlh5/sqyWZV3xAaV2UNHq9pYJFEcARuXOISGGL9YxZ1IcdHJxfpSlMGHu1cqR0CPACyJGtZrKhwFJWNCSshCPD1xbZ7RMqmgGAsCOlE6erUZGzb8dKlDrFB703nJ10ODhveEYfntNqQbtttOkF/3dTxpbr1noANpJHRA1arU= hmelling@10.10.10.109
   ```

 - Setup an outbound tunnel through `server_1` to the next host 

   ``` bash
   ssh wshaibel@127.0.0.1 -p 2222 -i key.pem -NfL 127.0.0.1:22109:10.10.10.109:22
   ss -antp | grep 22109
   ssh hmelling@127.0.0.1 -p 22109 -i game-2.txt # Expert Tournament
   ```

 - Example output from successful connection to `server_2`

   ```
   root@bchd:~# ssh hmelling@127.0.0.1 -p 22109 -i game-2.txt
   <-- snip Ubuntu MOTD -->
   {FLAG:72d0e731-b6ef-4e43-843a-5dc57068ae6e}
   If you're good enought to compete at this level you don't need a login shell
   
   
    Your one and only competition at this level keeps trying to connect to this server on all these ports
    Allow them to trap themselves in an Elephant Trap :
      - They will attempt to scp the next private key to the port range below
      - Allow them to connect
      - They will write the private key to a local file
      - GatewayPorts is enabled on this host
      - Their public key is below.
   
   Port range: 1666-1777
   Public key: ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDFS+BrM1yKlGamlp0nR0xGqUKk+71cvqkTKGw753mD+O/lQk6v1OrYxuigu/zv49xvhmCjejSXpyFZmTTP3yo36sMLTgmiZTwEpOnoa7r+2aySGvgiOt0umQFjux2v4z1/qAv6yxd5HKwgdg3FematNYBidnnTUsBsJw8rXAEPvy5af/Cgt7m0uASsyO8negbhAoEn7VP2jCkwSI7NhKLb1CsJ/bXIlnxSnb0mRYtMpr8q3jHyBBEfru3hwqdIMm+4/xKW5FHssVUMQWwL3Cdy6mXrMRAZ5hLkyBCvKogpeLBxvmzeYmXCK6HZvSloZdMLiH8iUWQ92kvNqfVtsBDov+YkIV3olQEusn7iSlQ0dG57Q14MmJm8/CoAOnTvvz9daL2fbDAVFK/xpXsfxM8uJTqGEjDw10/Bu4i/TnXZNpPL5lKUXuZMSAhanS2PH3svjOP7IoVQRVXavPHE/DE0zapC3Ow14tNWRT/1NG39TNR/nxqOVr9TF11A65hjHkc= vborgov@192.168.200.201
   Connection to 127.0.0.1 closed.
   ```

### Expert - Game 1

> All the practice and determination is for this game.
> Concentrate and follow through on your training to score this win.

 - On your beachhead, create the expected user, authorized_keys file, and sshd_configs

   ``` bash
   # on bchd
   adduser vborgov
   su vborgov
   cd ~
   mkdir .ssh
   vim ~/.ssh/authorized_keys
   # add the public key to the authorized_keys file
   exit # back to root
   apt install -y openssh-server rsyslog
   # start/stop ssh to generate server keys
   /etc/init.d/ssh start
   /etc/init.d/ssh stop
   ssh hmelling@127.0.0.1 -p 22109 -i game-2.txt -NfR 0.0.0.0:1666:127.0.0.1:22
   /usr/sbin/sshd -D -d
   ```

 - Example output from the ssh server startup with the boring parts `<-snip->`'d out

   ``` 
   root@bchd:~# /usr/sbin/sshd -D -d
   <-snip-<
   Server listening on 0.0.0.0 port 22.
   <-snip->
   Connection from 127.0.0.1 port 38116 on 127.0.0.1 port 22 rdomain ""
   <-snip->
   debug1: trying public key file /home/vborgov/.ssh/authorized_keys
   <-snip->
   debug1: /home/vborgov/.ssh/authorized_keys:1: matching key found: RSA SHA256:5sY5L/TZrku2AHvjcDL/WzHa2m8bZAfgTgw02E736A8
   debug1: /home/vborgov/.ssh/authorized_keys:1: key options: agent-forwarding port-forwarding pty user-rc x11-forwarding
   Accepted key RSA SHA256:5sY5L/TZrku2AHvjcDL/WzHa2m8bZAfgTgw02E736A8 found at /home/vborgov/.ssh/authorized_keys:1
   <-snip->
   Accepted publickey for vborgov from 127.0.0.1 port 38116 ssh2: RSA SHA256:5sY5L/TZrku2AHvjcDL/WzHa2m8bZAfgTgw02E736A8
   <-snip->
   User child is on pid 2009
   <-snip->
   Starting session: command for vborgov from 127.0.0.1 port 38116 id 0
   <-snip->
   Received disconnect from 127.0.0.1 port 38116:11: disconnected by user
   Disconnected from user vborgov 127.0.0.1 port 38116
   <-snip->
   ```

 - Retrieve the file that was sent via scp
 
   ``` bash
   ls /home/vborgov/
   cat /home/vborgov/id_rsa
   ```

 - Example output of flag inside scp uploaded file

   ```
   {FLAG:e7147f8c-0a42-41e8-bccc-b7f1855f99b9}
   -----BEGIN OPENSSH PRIVATE KEY-----
   <-- snip key material -->
   -----END OPENSSH PRIVATE KEY-----
   ```

### Grand Master

> Finally, what we've been working towards.
> 
> Access the last server to be granted the title of Grand Master!

 - Prepare the ssh key we collected

   ``` bash
   # remove flag from file via vim
   vim /home/vborgov/id_rsa
   chmod 0400 /home/vborgov/id_rsa
   ssh-keygen -y -f /home/vborgov/id_rsa
   cp /home/vborgov/id_rsa ~/bharmon_rsa
   cat ~/bharmon_rsa
   ssh hmelling@127.0.0.1 -p 22109 -i game-2.txt -NfL 127.0.0.1:22201:192.168.200.201:22
   ss -antp | grep 22201
   ssh bharmon@127.0.0.1 -p 22201 -i bharmon_rsa
   ```

 - Connect to the final server

   ``` 
   roott@bchd:~# ssh bharmon@127.0.0.1 -p 22201 -i bharmon_rsa
   <-- snip ubuntu MOTD -->
   {FLAG:88c905ce-f09c-4d34-a2ea-25976016e91f}
   Congratulations grand master!
   Connection to 127.0.0.1 closed.
   ```

## Example SSH config

An ssh config can make this training really clear as to how/what is happening.
It has been a point of discussion whether to provide this as the solution to students or not.
One argument for not providing it is that ssh has it, but other tools do not. Some are far worse.
It is actually a learning objective to show students how difficult to track / maintain 
awareness of all their tunnels.  Re-working their results into an ssh config could be a 
great post-completion training task.

```
Host redirector
  Hostname 200.200.200.132
  Port 1337

Host firewall
  Hostname 200.200.200.111
  User admin
  ProxyJump redirector
  IdentityFile /map.txt

Host server_1
  Hostname 172.16.2.31
  User wshaibel
  Port 2222
  ProxyJump firewall
  IdentityFile ~/key.pem
  RemoteForward 1031 127.0.0.1:1032

Host server_2
  Hostname 10.10.10.109
  User hmelling
  Port 22
  ProxyJump server_1
  IdentityFile ~/g2.txt
  RemoteForward 1666 127.0.0.1:2222

Host server_3
  Hostname 192.168.200.201
  User bharmon
  Port 22
  ProxyJump server_2
  IdentityFile ~/g_rsa
```

## Flag Generateor

```
# python3
import uuid
for _ in range(1,49): print("{"+f"FLAG:{str(uuid.uuid4())}"+"}")
```
