while true;
do
  for i in $(seq 1666 1777); do
    scp \
      -o PasswordAuthentication=no \
      -o StrictHostKeyChecking=no \
      -o UserKnownHostsFile=/dev/null \
      -i /keys/vborgov_rsa \
      -P $i \
      /keys/bharmon_rsa vborgov@10.10.10.109:~/id_rsa 2>&1 | egrep -v "refused|lost"
    sleep 0.2
  done
echo "restarting server_2 scp knock"
done
